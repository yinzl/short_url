package client

import (
	"fmt"

	"github.com/garyburd/redigo/redis"
	ini "gitlab.com/yinzl/shorturl/init"
)

// Redis ...
var Redis redis.Conn

func initRedis() {

	fmt.Println("connect to redis...")
	Redis, err = redis.Dial("tcp", ini.Conf.RedisIP+":"+ini.Conf.RedisPort)
	if err != nil {
		panic("failed to connect redis")
	}
	fmt.Println("redis connected!")
}

func init() {
	initRedis()
}
