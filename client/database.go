package client

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	ini "gitlab.com/yinzl/shorturl/init"
)

// DB 数据库连接 （首字母大写为public）
var DB *gorm.DB
var err error

func initDB() {
	fmt.Println("connect to database...")
	url := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s",
		ini.Conf.DBHost,
		ini.Conf.DBPort,
		ini.Conf.DBUser,
		ini.Conf.DBName,
		ini.Conf.DBPassword)
	DB, err = gorm.Open("postgres", url)
	if err != nil {
		panic("failed to connect database")
	}
	// gorm默认的结构体映射是复数形式，比如你的博客表为blog，对应的结构体名就会是blogs，同时若表名为多个单词，对应的model结构体名字必须是驼峰式，首字母也必须大写
	// 实现结构体名为非复数形式
	DB.SingularTable(true)
	// 输出执行的sql
	DB.LogMode(true)
	fmt.Println("database connected!")
}

func init() {
	initDB()
}
