package short_url

import "time"

type ShortUrl struct {
	ID        int
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
	Url       string
	ShortUrl  string
	Traffic   int64
	IsValid   bool
}
