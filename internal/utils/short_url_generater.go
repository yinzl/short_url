package utils

import (
	"crypto/md5"
	"encoding/hex"
	"strconv"

	"github.com/garyburd/redigo/redis"
	"gitlab.com/yinzl/shorturl/client"
)

const (
	VAL   = 0x3FFFFFFF
	INDEX = 0x0000003D
)

var (
	alphabet = []byte("abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ")
)

//Transform ...
func Transform(longURL string) ([4]string, error) {
	md5Str := getMd5Str(longURL)
	//var hexVal int64
	var tempVal int64
	var result [4]string
	var tempUri []byte
	for i := 0; i < 4; i++ {
		tempSubStr := md5Str[i*8 : (i+1)*8]
		hexVal, err := strconv.ParseInt(tempSubStr, 16, 64)
		if err != nil {
			return result, nil
		}
		tempVal = int64(VAL) & hexVal
		var index int64
		tempUri = []byte{}
		for i := 0; i < 6; i++ {
			index = INDEX & tempVal
			tempUri = append(tempUri, alphabet[index])
			tempVal = tempVal >> 5
		}
		result[i] = string(tempUri)
	}
	return result, nil
}

/** generate md5 checksum of URL in hex format **/
func getMd5Str(str string) string {
	m := md5.New()
	m.Write([]byte(str))
	c := m.Sum(nil)
	return hex.EncodeToString(c)
}

// GetShortURL ...
func GetShortURL() (string, error) {
	const REDISKEY = "SHORT_URL"
	const REDISVALUE = 46656 // 46656为36进制下的 1000
	redisValue, err := redis.Int(client.Redis.Do("GET", REDISKEY))
	if err != nil {
		_, err = client.Redis.Do("SET", REDISKEY, REDISVALUE)
		return strconv.FormatInt(REDISVALUE, 36), nil
	}
	redisValue++
	_, err = client.Redis.Do("SET", REDISKEY, redisValue)
	if err != nil {
		return "", err
	}
	return strconv.FormatInt(int64(redisValue), 36), nil
}
