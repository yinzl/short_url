package main

import (
	"log"
	"net/http"
	"os"

	"github.com/99designs/gqlgen/handler"
	short_url "gitlab.com/yinzl/shorturl"

	_ "gitlab.com/yinzl/shorturl/client"
	_ "gitlab.com/yinzl/shorturl/init"
)

const defaultPort = "8080"

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}

	http.Handle("/", handler.Playground("GraphQL playground", "/query"))
	http.Handle("/query", handler.GraphQL(short_url.NewExecutableSchema(short_url.Config{Resolvers: &short_url.Resolver{}})))

	log.Printf("connect to http://localhost:%s/ for GraphQL playground", port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
