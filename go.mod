module gitlab.com/yinzl/shorturl

go 1.13

require (
	github.com/99designs/gqlgen v0.10.2
	github.com/garyburd/redigo v1.6.0
	github.com/jinzhu/gorm v1.9.12
	github.com/microcosm-cc/bluemonday v1.0.2 // indirect
	github.com/pkg/errors v0.8.1
	github.com/russross/blackfriday v2.0.0+incompatible // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/shen100/golang123 v0.0.0-20181105022113-2d9d122019c9
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	github.com/vektah/gqlparser v1.2.0
	gopkg.in/ini.v1 v1.51.1
)
