package short_url

import (
	"context"
	"fmt"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/yinzl/shorturl/client"
	"gitlab.com/yinzl/shorturl/internal/utils"
) // THIS CODE IS A STARTING POINT ONLY. IT WILL NOT BE UPDATED WITH SCHEMA CHANGES.

// Resolver ...
type Resolver struct{}

// Mutation ...
func (r *Resolver) Mutation() MutationResolver {
	return &mutationResolver{r}
}

// Query ...
func (r *Resolver) Query() QueryResolver {
	return &queryResolver{r}
}

type mutationResolver struct{ *Resolver }

func (r *mutationResolver) CreateShortURL(ctx context.Context, input CreateShortURLInput) (*ShortUrl, error) {
	if len(input.URL) == 0 {
		return nil, errors.New("url can not be empty")
	}
	shortUrl, err := findShortURL("url", input.URL)
	// 已有为过期记录则返回
	if err == nil {
		return shortUrl, nil
	}
	// 有数据但是为无效状态，置为有效
	if shortUrl != nil {
		if err = client.DB.Model(&shortUrl).Update("isValid", true).Update("created_at", time.Now()).Error; err != nil {
			return nil, err
		}
		return shortUrl, nil
	}
	fmt.Println(input.URL)
	var strs, _ = utils.Transform(input.URL)
	newShortURL := &ShortUrl{Url: input.URL, ShortUrl: "/" + strs[0], Traffic: 0, IsValid: true}
	// id := uuid.NewV4()
	// 使用redis生成短网址
	// str, _ := utils.GetShortURL()
	// newShortURL := &ShortUrl{Url: input.URL, ShortUrl: "/" + str, Traffic: 0, IsValid: true}
	if err = client.DB.Create(newShortURL).Error; err != nil {
		return nil, err
	}
	return newShortURL, nil
}

type queryResolver struct{ *Resolver }

// VALIDTIME ...有效天数
const VALIDTIME = 3

func (r *queryResolver) ShortURL(ctx context.Context, input string) (*ShortUrl, error) {
	shortUrl, err := findShortURL("short_url", input)
	if err != nil {
		return nil, err
	}
	var traffic = shortUrl.Traffic
	var validTime = time.Now().AddDate(0, 0, -VALIDTIME)
	if shortUrl.CreatedAt.Before(validTime) {
		if err = client.DB.Model(&shortUrl).Update("isValid", false).Error; err != nil {
			return nil, err
		}
		return nil, errors.New("url has expired,add plz")
	}
	if err = client.DB.Model(&shortUrl).Update("Traffic", traffic+1).Error; err != nil {
		return nil, err
	}
	return shortUrl, nil
}

func findShortURL(colName string, input string) (*ShortUrl, error) {
	if len(input) == 0 {
		return nil, errors.New("url can not be empty")
	}
	var shortUrl ShortUrl
	if err := client.DB.Where(colName+"=?", input).Find(&shortUrl).Error; err != nil {
		return nil, err
	}
	if !shortUrl.IsValid {
		// 无效时返回并提示已经无效
		return &shortUrl, errors.New("url has expired,add plz")
	}
	return &shortUrl, nil
}

func (r *queryResolver) Traffic(ctx context.Context, input string) (int, error) {
	shortUrl, err := findShortURL("short_url", input)
	// 此处错误无需处理，只需关注shorUrl是否返回
	if shortUrl != nil {
		return int(shortUrl.Traffic), nil
	}
	return 0, err
}

func (r *queryResolver) ShortUrls(ctx context.Context) ([]*ShortUrl, error) {
	var shortUrls []*ShortUrl
	if err := client.DB.Find(&shortUrls).Error; err != nil {
		return nil, err
	}
	return shortUrls, nil
}
